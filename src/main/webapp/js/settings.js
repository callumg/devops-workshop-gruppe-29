document.getElementById("newUsername").value = sessionStorage.getItem("username");
/* Adds password to session storage (treng ikke denne?) (1)
document.getElementById("newPassword").value = sessionStorage.getItem("password"); */
document.getElementById("cancelButton").addEventListener("click", function(){
    window.location.href = "../app.html";
});

/**
 * Makes HTTP PUT request to server for updating username and password
 */
function editUser (event) {
    event.preventDefault();
    let newInformation = {
        /* Update variable 'username' and 'password' (0) */
        "username": document.getElementById("newUsername").value,
        "password": document.getElementById("newPassword").value
    };
    /* Updates username (but not password?) (2) */
    function loadApplication(user){
    sessionStorage.setItem("username", user.username);
    sessionStorage.setItem("password", user.password);

    }

    fetch('../api/user/'+sessionStorage.getItem("userId"), {
        method: "PUT",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify(newInformation)
    })
        .then(response => response.json())
        .then(response => {
            if (response === true) {
                alert("Bruker oppdatert");
                window.location.href = "../app.html";
            } else {
                alert("Brukernavn eksisterer fra før, vennligst skriv inn et nytt brukernavn");
            }
        })
        .catch(error => console.error(error));
}
