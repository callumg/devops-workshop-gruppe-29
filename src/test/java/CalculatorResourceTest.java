import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals("400", calculatorResource.calculate(expression));

        expression = "100+300+400";
        assertEquals("800", calculatorResource.calculate(expression));

        expression = " 300 - 99 ";
        assertEquals("201", calculatorResource.calculate(expression));

        expression = " 300 - 99 - 101";
        assertEquals("100", calculatorResource.calculate(expression));

        expression = "20*20";
        assertEquals("400", calculatorResource.calculate(expression));

        expression = "20*20*2";
        assertEquals("800", calculatorResource.calculate(expression));

        expression = "7/7";
        assertEquals("1", calculatorResource.calculate(expression));

        expression = "70/7/10";
        assertEquals("1", calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+99+1";
        assertEquals(400, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2-4";
        assertEquals(14, calculatorResource.subtraction(expression));
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "5*6";

        assertEquals(30, calculatorResource.multiplication(expression));

        expression = "5*6*3";

        assertEquals(90, calculatorResource.multiplication(expression));
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100/10";

        assertEquals(10, calculatorResource.division(expression));

        expression = "100/10/10";

        assertEquals(1, calculatorResource.division(expression));
    }
}
